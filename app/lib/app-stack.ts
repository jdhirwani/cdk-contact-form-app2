import * as cdk from '@aws-cdk/core';
import lambda =require('@aws-cdk/aws-lambda');
import * as apigateway from '@aws-cdk/aws-apigateway';
import * as path from 'path';
import * as sns from '@aws-cdk/aws-sns';
import * as ssm from '@aws-cdk/aws-ssm';
import * as subs from '@aws-cdk/aws-sns-subscriptions';

export interface ApiGatewayProps extends cdk.StackProps{
  stage: string,
  client: string,
  parameterPath: string

};

export class AppStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: ApiGatewayProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
    //define the AWS Lambda Handler

    const lambdaProps : lambda.FunctionProps = {
      runtime: lambda.Runtime.NODEJS_10_X,   //execution environment
      code: lambda.Code.fromAsset('lambda'),//(path.join(__dirname, '../lambda/')), //code loaded from "lambda" directory
      handler: 'contactForm.handler',   //file is contactForm , function is handler
      environment: {
        "client": props.client,
        "stage": props.stage,
        "parameterPath": props.parameterPath

      }
    };

    const contactFormHandler = new lambda.Function(this, 'contactFormLambdaFunction', lambdaProps );
    

const contactFormApi = new apigateway.LambdaRestApi(this, 'contactFormEndpoint', {
  handler: contactFormHandler,
  proxy: false
});

const form = contactFormApi.root.addResource('contact-form');
form.addMethod('GET');   // Get /items
form.addMethod('POST');   // POST /items

const apiUrl = contactFormApi.url + form.path;
new cdk.CfnOutput(this, 'apiUrl',{
  value: apiUrl
});

 // define SNS for the contact form
 const topicName: string = "contactForm" + "_" + props.stage;
 const snsTopic: sns.Topic = new sns.Topic(this, topicName,{
   displayName: topicName
 });

 let ssmParameterName = props.parameterPath + '/contactFormSNSTopicArn';
const snsTopicParameter = new ssm.StringParameter(this, 'contactFormSNSTopicArn', {
  allowedPattern: '.*',
  description: 'The SNS topic to which the contact form lambda hander should send the message to.',
  parameterName: ssmParameterName,
  stringValue: snsTopic.topicArn,
  tier: ssm.ParameterTier.STANDARD
});

snsTopic.addSubscription(new subs.EmailSubscription("jatandar.dhirwani@gmail.com"));

snsTopicParameter.grantRead(contactFormHandler);
snsTopic.grantPublish(contactFormHandler);

  }
}
