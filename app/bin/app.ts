#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import { AppStack, ApiGatewayProps } from '../lib/app-stack';

const app = new cdk.App();
const clientVal = app.node.tryGetContext('client') || 'internal'; //the default value is 'internal'
const stageVal = app.node.tryGetContext('stage') || 'dev'; //the default value is developer
const developerNameVal = app.node.tryGetContext('developername') || process.env.name || '';  // leave blank if hte username cannot be derived

var parameterPath: string = '/' + clientVal + '/' + stageVal ;

const props: ApiGatewayProps = {
    tags: {
        "client": clientVal,
        "stage": stageVal
    },
    stage: stageVal,
    client: clientVal,
    parameterPath: parameterPath.toLowerCase()
};
new AppStack(app, 'ContactForm', props);
app.synth();
